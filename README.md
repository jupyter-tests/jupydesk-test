# jupydesk-test

Some notebooks with different kernels to test the [jupydesk](https://jupyterhub-sam.inria.fr:8000) instance.

Here are some tips to work in Jupyterhub.


## Create your conda environment and relative python kernel

Open a new terminal (`Launcher - Other - Terminal`) and use it to create your conda environment. You have to create it locally if you want it to be persistant over server shutdown:

```bash
conda create --prefix=$HOME/.conda/myenv
```

Then activate your environment and install your dependencies:

```bash
source ~/.bashrc # Not sourced by default
conda activate myenv
conda install anycondapackages
```

To be able to use your environment as a python kernel you have to install ipykernel:

```bash
conda install ipykernel
```

Then your environment can be used as a kernel in the `Launcher` as `Python [conda env:myenv]`.

## Create your Julia kernel

Julia is already installed with the [IJulia](https://julialang.github.io/IJulia.jl/stable/manual/installation/) package. So you can just install the packages you need and using:

```Julia
using IJulia
installkernel("MyJuliaKernel")

```

You should see your kernel in the list of the `Launcher`




